﻿Imports CommandLine
Imports CommandLine.Text

Public Class Options

    <[Option]("console", HelpText:="Start the service with the console window open")>
    Public Property Console As Boolean

    <ParserState()>
    Public Property LastParserState As IParserState

    <HelpOption("servicehelp")>
    Public Function GetUsage() As String

        Dim help = New HelpText()
        help.Heading = New HeadingInfo(GetAppName, GetVersion)
        help.Copyright = New CopyrightInfo("Redgum Technologies Pty Ltd", 2014)
        help.AdditionalNewLineAfterOption = True
        help.AddDashesToOption = True

        help.AddPreOptionsLine("Usage: app --console")
        help.AddOptions(Me)
        Return help
    End Function

    Private Function GetAppName() As String
        Return "Redgum SMDR Relay Service"
    End Function

    Private Function GetVersion() As String
        Return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()
    End Function
End Class
