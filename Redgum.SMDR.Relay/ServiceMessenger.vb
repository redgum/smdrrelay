﻿
Public Enum ServiceMessageType
    [Error] = 0
    Warning = 1
    Information = 2
    Debug = 3
End Enum

Public Class ServiceMessage

    Public Sub New(message As String, messageType As ServiceMessageType)
        MyBase.New()
        Me._Message = message
        Me._Type = messageType
        Me._DateLogged = Date.Now
    End Sub

    Private _Type As ServiceMessageType
    Public ReadOnly Property Type As ServiceMessageType
        Get
            Return Me._Type
        End Get
    End Property

    Private _DateLogged As DateTime
    Public ReadOnly Property DateLogged As DateTime
        Get
            Return Me._DateLogged
        End Get
    End Property

    Private _Message As String
    Public ReadOnly Property Message As String
        Get
            Return Me._Message
        End Get
    End Property

End Class
