﻿Imports System.Net
Imports System.Text
Imports System.Threading

Public Class SMDRRelayTarget

    'Private _Socket As Sockets.Socket
    Private _MessageHandler As Action(Of ServiceMessage)

    Private _EndpointIPAddress As IPAddress
    Private _EndpointPort As Integer = 0
    Private _ConnectionTimeout As TimeSpan
    Private _Cache As SMDRRelayCache

    Public Sub New(endpointIPAddress As String, endpointPort As Integer, connectionTimeout As TimeSpan, messageHandler As Action(Of ServiceMessage))
        MyBase.New()
        Me._MessageHandler = messageHandler
        _Cache = New SMDRRelayCache(My.Settings.AppDataFolder, String.Format("cache-{0}-{1}", endpointIPAddress, endpointPort), My.Settings.CacheLimit)
        InitialiseSMDRRelay(endpointIPAddress, endpointPort, connectionTimeout)

    End Sub

    Private Sub InitialiseSMDRRelay(endpointIPAddress As String, endpointPort As Integer, connectionTimeout As TimeSpan)
        If Not String.IsNullOrWhiteSpace(endpointIPAddress) AndAlso endpointPort > 0 Then

            _EndpointIPAddress = IPAddress.Any
            _EndpointPort = endpointPort
            _ConnectionTimeout = connectionTimeout
            If Not String.IsNullOrWhiteSpace(endpointIPAddress) Then
                If Not IPAddress.TryParse(endpointIPAddress, _EndpointIPAddress) Then
                    _EndpointIPAddress = IPAddress.Any
                End If
            End If
            SendServiceMessage(String.Format("Registered relay endpoint at {0}:{1}", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Information)

        End If
    End Sub

    Private Sub TryResendCache()

        If _Cache.Cache.Any Then

            Dim sentItems As New List(Of SMDRRelayCacheItem)
            Dim updateItems As New List(Of SMDRRelayCacheItem)

            Dim sock = OpenSocket()

            If sock IsNot Nothing AndAlso sock.Connected Then
                For Each item In _Cache.Cache
                    item.SendAttempts += 1
                    'Stack the item as updated so the 
                    'cache can be updated with new data post send process
                    updateItems.Add(item)
                    'Try the send
                    Try
                        If sock IsNot Nothing AndAlso sock.Connected Then
                            SendServiceMessage("Sending data to endpoint. Data is: " & item.Data, ServiceMessageType.Debug)
                            Send(sock, Encoding.UTF8.GetBytes(item.Data), 0, item.Data.Length, 10000)
                            'remove the item from the cache if it was created
                            'as part of the send process. If it was passed in, the we
                            'don't remove it as the external process will deal
                            SendServiceMessage(String.Format("Closing connection to endpoint at {0}:{1}...", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Information)
                            sentItems.Add(item)
                        End If
                    Catch ex As Exception
                        SendServiceMessage(String.Format("Send to relay endpoint {0}:{1} failed. Reason: {2}", _EndpointIPAddress.ToString, _EndpointPort.ToString, ex.GetBaseException.Message), ServiceMessageType.Error)
                        Exit For
                    End Try
                Next
                If updateItems.Any Then
                    _Cache.Cache.Update(updateItems)
                End If
                If sentItems.Any Then
                    _Cache.Cache.Remove(sentItems)
                End If

                'Lastly, close the socket
                ForceCloseSocket(sock)
            End If

        End If
    End Sub

    Public Sub SendNotificationToSMDRRelay(rawContents As String)

        _Cache.AddRecord(rawContents)

        TryResendCache()

    End Sub

    'Private Function InternalSendNotificationToSMDRRelay(item As SMDRRelayCacheItem) As Boolean
    'If we have an initialised socket, then we can send

    'Cache the item first
    'Dim sock = OpenSocket()

    'Dim sock As Sockets.Socket = Nothing
    'SendServiceMessage(String.Format("Connecting to endpoint at {0}:{1}...", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Information)
    'Try
    '    sock = New Sockets.Socket(_EndpointIPAddress.AddressFamily, Sockets.SocketType.Stream, Sockets.ProtocolType.Tcp)

    '    'Force the connect timeout to be only a couple of seconds
    '    Dim result As IAsyncResult = sock.BeginConnect(New IPEndPoint(_EndpointIPAddress, _EndpointPort), Nothing, Nothing)
    '    Dim timeout As Integer
    '    If _ConnectionTimeout.Milliseconds > 0 Then
    '        timeout = _ConnectionTimeout.Milliseconds
    '    Else
    '        timeout = 5000
    '    End If
    '    result.AsyncWaitHandle.WaitOne(timeout, True)

    '    If Not sock.Connected Then
    '        'Force close 
    '        sock.Close()
    '        SendServiceMessage(String.Format("Failed to connect to endpoint at {0}:{1}. Timeout", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Error)
    '        Return False
    '    Else
    '        SendServiceMessage(String.Format("Connected to endpoint at {0}:{1}", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Information)
    '    End If

    'Catch nullEx As ArgumentNullException
    '    SendServiceMessage(String.Format("Failed to connect to endpoint at {0}:{1}. Endpoint is null", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Error)
    '    Return False
    'Catch sckEx As Sockets.SocketException
    '    SendServiceMessage(String.Format("Failed to connect to endpoint at {0}:{1}. Socket exception: {2}", _EndpointIPAddress.ToString, _EndpointPort.ToString, sckEx.GetBaseException.Message), ServiceMessageType.Error)
    '    Return False
    'Catch ex As Exception
    '    'Probably timeouut
    '    SendServiceMessage(String.Format("Failed to connect to endpoint at {0}:{1}. Unhandled exception: {2}", _EndpointIPAddress.ToString, _EndpointPort.ToString, ex.GetBaseException.Message), ServiceMessageType.Error)
    '    Return False
    'End Try

    'Try
    '    If sock IsNot Nothing AndAlso sock.Connected Then
    '        Send(sock, Encoding.UTF8.GetBytes(rawContents), 0, rawContents.Length, 10000)

    '        SendServiceMessage(String.Format("Closing connection to endpoint at {0}:{1}...", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Information)
    '    End If
    'Catch ex As Exception
    '    SendServiceMessage(String.Format("Send to relay endpoint {0}:{1} failed. Reason: {2}", _EndpointIPAddress.ToString, _EndpointPort.ToString, ex.GetBaseException.Message), ServiceMessageType.Error)
    '    Return False
    'End Try
    'ForceCloseSocket(sock)
    'End Function

    Private Function OpenSocket() As Sockets.Socket

        Dim sock As Sockets.Socket = Nothing
        SendServiceMessage(String.Format("Connecting to endpoint at {0}:{1}...", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Information)
        Try
            sock = New Sockets.Socket(_EndpointIPAddress.AddressFamily, Sockets.SocketType.Stream, Sockets.ProtocolType.Tcp)

            'Force the connect timeout to be only a couple of seconds
            If _ConnectionTimeout.Milliseconds > 0 Then
                sock.SendTimeout = _ConnectionTimeout.Milliseconds
                sock.ReceiveTimeout = _ConnectionTimeout.Milliseconds
            Else
                sock.SendTimeout = 5000
                sock.ReceiveTimeout = 5000
            End If

            'Dim result As IAsyncResult = sock.BeginConnect(New IPEndPoint(_EndpointIPAddress, _EndpointPort), Nothing, Nothing)
            sock.Connect(New IPEndPoint(_EndpointIPAddress, _EndpointPort))

            'Dim timeout As Integer
            'If _ConnectionTimeout.Milliseconds > 0 Then
            '    timeout = _ConnectionTimeout.Milliseconds
            'Else
            '    timeout = 5000
            'End If
            'result.AsyncWaitHandle.WaitOne(timeout, True)

            If Not sock.Connected Then
                'Force close 
                ForceCloseSocket(sock)
                SendServiceMessage(String.Format("Failed to connect to endpoint at {0}:{1}. Timeout", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Error)
                Return Nothing
            Else
                SendServiceMessage(String.Format("Connected to endpoint at {0}:{1}", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Information)
            End If
            Return sock

        Catch nullEx As ArgumentNullException
            ForceCloseSocket(sock)
            SendServiceMessage(String.Format("Failed to connect to endpoint at {0}:{1}. Endpoint is null", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Error)
            Return Nothing
        Catch sckEx As Sockets.SocketException
            ForceCloseSocket(sock)
            SendServiceMessage(String.Format("Failed to connect to endpoint at {0}:{1}. Socket exception: {2}", _EndpointIPAddress.ToString, _EndpointPort.ToString, sckEx.GetBaseException.Message), ServiceMessageType.Error)
            Return Nothing
        Catch ex As Exception
            'Probably timeout
            ForceCloseSocket(sock)
            SendServiceMessage(String.Format("Failed to connect to endpoint at {0}:{1}. Unhanded exception: {2}", _EndpointIPAddress.ToString, _EndpointPort.ToString, ex.GetBaseException.Message), ServiceMessageType.Error)
            Return Nothing
        End Try
    End Function

    Private Sub ForceCloseSocket(sock As Sockets.Socket)
        Try
            If sock IsNot Nothing AndAlso sock.Connected Then
                sock.Close()
                SendServiceMessage(String.Format("Connection to endpoint at {0}:{1} closed", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Information)
            End If
        Catch ex As Exception
            'Failed to close the socket, but we really don't care
            SendServiceMessage(String.Format("Error closing socket at {0}:{1} - {2}", _EndpointIPAddress.ToString, _EndpointPort.ToString, ex.GetBaseException.Message), ServiceMessageType.Error)
        End Try
    End Sub

    Public Function Send(socket As Sockets.Socket, buffer As Byte(), offset As Integer, size As Integer, timeout As Integer) As Boolean

        Dim startTickCount As Integer = Environment.TickCount
        Dim sent As Integer = 0

        SendServiceMessage(String.Format("Relaying to endpoint at {0}:{1}", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Information)

        ' how many bytes is already sent
        Do
            'If Environment.TickCount > startTickCount + timeout Then
            '    SendServiceMessage(String.Format("Timeout occurred sending to {0}:{1}", _EndpointIPAddress.ToString, _EndpointPort.ToString), ServiceMessageType.Warning)
            'End If
            Try
                sent += socket.Send(buffer, offset + sent, size - sent, Sockets.SocketFlags.None)
            Catch ex As Sockets.SocketException
                If ex.SocketErrorCode = Sockets.SocketError.WouldBlock OrElse
                    ex.SocketErrorCode = Sockets.SocketError.IOPending OrElse
                    ex.SocketErrorCode = Sockets.SocketError.NoBufferSpaceAvailable Then
                    ' socket buffer is probably full, wait and try again
                    Thread.Sleep(30)
                Else
                    ' any serious error occur
                    SendServiceMessage(ex.GetBaseException.Message, ServiceMessageType.Error)
                    Return False
                End If
            End Try
        Loop While sent < size
        Return True
    End Function

    Private Sub SendServiceMessage(message As String, type As ServiceMessageType)
        If _MessageHandler IsNot Nothing Then
            Me._MessageHandler.Invoke(New ServiceMessage(String.Format("{0}:{1} - {2}", _EndpointIPAddress.ToString, _EndpointPort.ToString, message), type))
        Else
            'No message handler so direct write
            LogFileWriter.WriteLogFileEntry(type, message)
        End If
    End Sub

    Public Overrides Function ToString() As String
        Return String.Format("{0}:{1}", _EndpointIPAddress.ToString, _EndpointPort.ToString)
    End Function
End Class
