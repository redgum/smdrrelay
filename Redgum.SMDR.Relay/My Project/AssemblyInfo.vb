﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Redgum.SMDR.Relay")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Redgum Technologies Pty Ltd")> 
<Assembly: AssemblyProduct("Redgum.SMDR.Relay")> 
<Assembly: AssemblyCopyright("Copyright © 2014-2015 Redgum Technologies Pty Ltd")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("0a08f9fd-32ca-4d72-ba8d-d4d47c9d082d")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.7.0")>
<Assembly: AssemblyFileVersion("1.0.7.0")>
