﻿Imports System.IO

Module ExceptionWriter

    Public Sub WriteException(source As String, ex As Exception, Optional byPassLogFileWriter As Boolean = False)

        'Writes the exception to disk
        If Not byPassLogFileWriter Then
            Try
                LogFileWriter.WriteLogFileEntry(ServiceMessageType.Error, "Exception: " & source & " : " & ex.GetBaseException.Message)
                LogFileWriter.WriteLogFileEntry(ServiceMessageType.Error, "Exception: " & ex.StackTrace)
            Catch logEx As Exception
                'Failed miserably
            End Try
        End If
        Try
            Dim appData = IO.Path.Combine(My.Settings.AppDataExceptionsFolder, String.Format("exception-{0}-{1}.log", source, DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-fff")))
            System.IO.File.WriteAllText(appData, ex.ToString)

        Catch writeEx As Exception
            'Failed miserably
        End Try


    End Sub
End Module

Public Class LogFileWriter

    Private Const MaxLogCount As Integer = 3
    Private Const MaxLogSize As Integer = 5000000
    Private Const AppName As String = "Redgum.SMDRRelay"

    Private Shared Function RollLogFile() As String
        Dim logFilePath As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), AppName)
        'Check if the directory exists, and create if not
        'Don't catch the failed to create directory error - let
        'it bubble up to the parent call and get handled
        If Not IO.Directory.Exists(logFilePath) Then
            IO.Directory.CreateDirectory(logFilePath)
        End If


        Dim wildLogName As String = String.Format("{0}*.log", AppName)

        Dim fileCounter As Integer = 0
        Dim logFileList As String() = Directory.GetFiles(logFilePath, wildLogName, SearchOption.TopDirectoryOnly)
        If logFileList.Length > 0 Then
            Array.Sort(logFileList, 0, logFileList.Length)
            fileCounter = logFileList.Length - 1
            'Make sure we apply the MaxLogCount (but only once to reduce the delay) 
            If logFileList.Length > MaxLogCount Then
                'Too many files - remove one and rename the others
                Try
                    File.Delete(logFileList(0))
                Catch ex As Exception
                    'Failed to delete the file
                End Try
                For i As Integer = 1 To logFileList.Length - 1
                    Try
                        File.Move(logFileList(i), logFileList(i - 1))
                    Catch ex As Exception
                        'Failed to rename the file up a number
                    End Try
                Next
                fileCounter -= 1
            End If

            Dim currFilePath As String = logFileList(fileCounter)

            Dim f As New FileInfo(currFilePath)
            If f.Length < MaxLogSize Then
                'still room in the current file
                Return currFilePath
            Else
                'need another filename
                fileCounter += 1

            End If
        End If
        Return String.Format("{0}{1}{2}{3:00}.log", logFilePath, Path.DirectorySeparatorChar, AppName, fileCounter)
    End Function

    Public Shared Sub WriteLogFileEntry(messageType As ServiceMessageType, message As String)
        Dim logFileName As String = RollLogFile()
        Dim msg As String = String.Empty
        Select Case messageType
            Case ServiceMessageType.Error
                msg = "[ERR] " & message
            Case ServiceMessageType.Debug
                msg = "[DBG] " & message
            Case ServiceMessageType.Information
                msg = "[INF] " & message
            Case ServiceMessageType.Warning
                msg = "[WRN] " & message
        End Select
        Try
            Using sw As New StreamWriter(logFileName, True)
                sw.AutoFlush = True
                sw.WriteLine(String.Format("{0:u} {1}", DateTime.Now, msg))
            End Using
        Catch ex As Exception
            Console.WriteLine("Error writing to rolling log file")
            Console.WriteLine(ex.GetBaseException.Message)
            Console.WriteLine(ex.StackTrace)
            WriteException("WriteLogFileEntry", ex, True)
        End Try
    End Sub
End Class