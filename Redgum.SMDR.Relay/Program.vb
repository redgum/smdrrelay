﻿Imports Topshelf

Module Program

    Sub Main(args As String())
        Dim startupHandled As Boolean = False

        Dim options = New Options
        Dim parser = New CommandLine.Parser

        If parser.ParseArguments(args, options) Then
            If args.Length = 0 Then
                startupHandled = True
                Console.WriteLine(options.GetUsage())
                Console.ReadKey()
            ElseIf options.Console Then
                startupHandled = True
                RunTaskServiceInConsoleMode()
            End If
        End If

        If Not startupHandled Then
            LogFileWriter.WriteLogFileEntry(ServiceMessageType.Information, "Startup commandline arguments not handled. Treating as a service startup")
            Dim host = HostFactory.Run(
                Sub(x)
                    LogFileWriter.WriteLogFileEntry(ServiceMessageType.Information, "Starting service in service mode")
                    x.Service(Of ServiceController)(
                        Sub(s)
                            s.ConstructUsing(Function(name) New ServiceController)
                            s.WhenStarted(Sub(tc) tc.Start())
                            s.WhenStopped(Sub(tc) tc.Stop())
                        End Sub)
                    x.RunAsNetworkService()
                    x.StartAutomaticallyDelayed()
                    x.SetDescription("SMDR relay/splitter to multiple SMDR endpoints")
                    x.SetDisplayName("Redgum SMDR Relay Service")
                    x.SetServiceName("Redgum.SMDR.Relay.Service")
                    x.EnableServiceRecovery(Sub(rc)
                                                rc.RestartService(5)
                                                rc.SetResetPeriod(0)
                                            End Sub)
                    LogFileWriter.WriteLogFileEntry(ServiceMessageType.Information, "Service started")
                End Sub)
        End If
    End Sub


    Private Sub RunTaskServiceInConsoleMode()
        Dim consoleService = New ServiceController

        LogFileWriter.WriteLogFileEntry(ServiceMessageType.Information, "Starting service in console mode")
        consoleService.Start()
        LogFileWriter.WriteLogFileEntry(ServiceMessageType.Information, "Service started")
        Dim lKeyInfo = Console.ReadKey()
        If lKeyInfo.Modifiers = ConsoleModifiers.Control AndAlso lKeyInfo.Key = ConsoleKey.C Then
            'Can exit
            LogFileWriter.WriteLogFileEntry(ServiceMessageType.Information, "Exiting service in console mode")
        Else

        End If
        consoleService.Stop()
        consoleService.Dispose()
    End Sub
End Module

Public Class ServiceController
    Implements IDisposable
    
    Private _Endpoint As SMDREndpoint
    Public Sub New()
        MyBase.New()

        _Endpoint = New SMDREndpoint(My.Settings.ListenOnIPAddress, My.Settings.ListenOnPort, My.Settings.ConnectionTimeout, My.Settings.RelayEndpoints, AddressOf HandleServiceMessageReceived)

    End Sub

    Public Sub Start()
        If _Endpoint IsNot Nothing Then
            _Endpoint.Start()
        End If
    End Sub

    Public Sub [Stop]()
        If _Endpoint IsNot Nothing Then
            _Endpoint.Stop()
        End If
    End Sub

    Private Sub HandleServiceMessageReceived(message As ServiceMessage)

        If message IsNot Nothing Then

            Console.ResetColor()
            Select Case message.Type
                Case ServiceMessageType.Error
                    Console.ForegroundColor = ConsoleColor.Red
                Case ServiceMessageType.Debug
                    Console.ForegroundColor = ConsoleColor.Gray
                Case ServiceMessageType.Information
                    'Console.ForegroundColor = ConsoleColor.White
                Case ServiceMessageType.Warning
                    Console.ForegroundColor = ConsoleColor.Yellow
            End Select
            Console.WriteLine(message.Message)
            LogFileWriter.WriteLogFileEntry(message.Type, message.Message)
            Debug.WriteLine("[SMDR] {0} - {1}", message.Type.ToString, message.Message)
        End If
    End Sub


#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class