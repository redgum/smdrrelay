﻿Imports System.Threading
Imports System.Net
Imports System.ComponentModel
Imports System.Linq

Public Class SMDREndpoint
    Implements IDisposable

    Private eventSource As String
    'Private _ListenThread As Thread

    Private _TcpListener As Net.Sockets.TcpListener
    Private _workerThread As BackgroundWorker

    Private _Relays As List(Of SMDRRelayTarget)
    Private _MessageHandler As Action(Of ServiceMessage)

    Private _ListeningIPAddress As IPAddress
    Private _ListeningTCPPort As Integer

    Public Sub New(listenOnIP As String, tcpPort As Integer, connectionTimeout As TimeSpan, relayEndpoints As System.Collections.Specialized.StringCollection, messageHandler As Action(Of ServiceMessage))
        MyBase.New()
        Try
            Me._MessageHandler = messageHandler
            Me.eventSource = "Redgum.SMDR.Relay Logging"

            _workerThread = New BackgroundWorker()
            _workerThread.WorkerSupportsCancellation = True
            AddHandler _workerThread.DoWork, AddressOf DoWork

            Try
                If (Not EventLog.Exists(Me.eventSource)) Then
                    EventLog.CreateEventSource(Me.eventSource, "Application")
                End If
            Catch ex As Exception
            End Try

            _ListeningTCPPort = tcpPort
            _ListeningIPAddress = IPAddress.Any
            If Not String.IsNullOrWhiteSpace(listenOnIP) Then
                If Not IPAddress.TryParse(listenOnIP, _ListeningIPAddress) Then
                    _ListeningIPAddress = IPAddress.Any
                End If
            End If

            SendServiceMessage(String.Format("Endpoint listening on {0}:{1}", _ListeningIPAddress.ToString, tcpPort), ServiceMessageType.Information)

            If relayEndpoints.Count() > 0 Then
                SendServiceMessage(String.Format("Registering {0} endpoints", relayEndpoints.Count()), ServiceMessageType.Information)
                'Build the relays
                _Relays = New List(Of SMDRRelayTarget)
                For Each address In relayEndpoints
                    Try
                        SendServiceMessage(String.Format("{0}: Attempting to register", address), ServiceMessageType.Information)
                        'Format is IP:Port eg: 192.168.1.1:3000
                        If Not String.IsNullOrWhiteSpace(address) AndAlso address.Contains(":") Then
                            Dim lIPAddress = address.Split(":"c)(0)
                            Dim lPort As Integer = 0
                            If Integer.TryParse(address.Split(":"c)(1), lPort) Then
                                Dim lRelay As New SMDRRelayTarget(lIPAddress, lPort, connectionTimeout, _MessageHandler)
                                _Relays.Add(lRelay)
                            End If
                        End If
                    Catch ex As Exception
                        SendServiceMessage(String.Format("{0}: Endpoint registration failed", address), ServiceMessageType.Error)
                        SendServiceMessage(String.Format("{0}: Exception: {1}", address, ex.GetBaseException().Message), ServiceMessageType.Error)
                        ExceptionWriter.WriteException("EndpointNew", ex)
                    End Try

                Next
            Else
                SendServiceMessage(String.Format("No endpoints found to register"), ServiceMessageType.Warning)
            End If

        Catch ex As Exception
            SendServiceMessage(String.Format("EndpointNew: {0}", ex.GetBaseException().Message), ServiceMessageType.Error)
            ExceptionWriter.WriteException("EndpointNew", ex)
        End Try
    End Sub

    Public Sub Start()
        StartListening()
    End Sub

    Public Sub [Stop]()
        StopListening()
    End Sub

    Private Sub StartListening()
        Try
            Me._TcpListener = New Net.Sockets.TcpListener(_ListeningIPAddress, _ListeningTCPPort)
            Me._TcpListener.Start()
            _workerThread.RunWorkerAsync()

            SendServiceMessage("Started listening", ServiceMessageType.Information)
        Catch ex As Exception
            ExceptionWriter.WriteException("StartListening", ex)
        End Try
    End Sub

    Private Sub StopListening()
        Try
            If _workerThread IsNot Nothing AndAlso _workerThread.IsBusy Then
                _workerThread.CancelAsync()
            End If
            Me._TcpListener.Stop()

            SendServiceMessage("Stopped listening", ServiceMessageType.Information)
        Catch ex As Exception
            ExceptionWriter.WriteException("StopListening", ex)
        End Try
    End Sub

    Private Sub DoWork(sender As Object, e As DoWorkEventArgs)
        While Not _workerThread.CancellationPending
            Try
                SendServiceMessage("Starting listener thread", ServiceMessageType.Information)
                Dim lTcpClient As Sockets.TcpClient = Me._TcpListener.AcceptTcpClient()
                Dim lThread As Thread = New Thread(AddressOf HandleClientCommunicationReceived)
                lThread.Start(lTcpClient)

            Catch ex As Exception
                ExceptionWriter.WriteException("DoWork", ex)
            End Try
        End While
    End Sub

    Private Sub HandleClientCommunicationReceived(client As Object)
        SendServiceMessage("Client communication received", ServiceMessageType.Information)
        Try
            Dim lTcpClient As Sockets.TcpClient = DirectCast(client, Sockets.TcpClient)
            Using lTcpClient
                Dim stream As Sockets.NetworkStream = lTcpClient.GetStream()
                Using stream
                    Dim numArray As Byte()
                    ReDim numArray(4096)
                    While True
                        Dim num As Integer = 0
                        Try
                            num = stream.Read(numArray, 0, 4096)
                        Catch
                            Exit While
                        End Try
                        If (num = 0) Then
                            Exit While
                        End If
                        Dim aSCIIEncoding As Text.ASCIIEncoding = New Text.ASCIIEncoding()
                        Dim str As String = aSCIIEncoding.GetString(numArray, 0, num)

                        SendServiceMessage(String.Format("SMDR message received at {0}", DateTime.Now), ServiceMessageType.Information)
                        SendServiceMessage(String.Format("SMDR message contents: {0}", str), ServiceMessageType.Information)
                        'Push to the relays
                        If _Relays IsNot Nothing Then
                            For Each relayItem In _Relays
                                SendServiceMessage("Relaying message to endpoint " & relayItem.ToString, ServiceMessageType.Information)
                                relayItem.SendNotificationToSMDRRelay(str)
                            Next
                        Else
                            SendServiceMessage("Lost relays... ", ServiceMessageType.Error)
                        End If
                    End While
                End Using
                lTcpClient.Close()
            End Using
        Catch ex As Exception
            ExceptionWriter.WriteException("ClientCommsReceived", ex)
        End Try
    End Sub

    Private Sub SendServiceMessage(message As String, type As ServiceMessageType)
        If _MessageHandler IsNot Nothing Then
            Me._MessageHandler.Invoke(New ServiceMessage(message, type))
        Else
            LogFileWriter.WriteLogFileEntry(type, message)
        End If
    End Sub

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
                _workerThread.Dispose()

            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
            _TcpListener = Nothing
        End If
        disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
    Protected Overrides Sub Finalize()
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(False)
        MyBase.Finalize()
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Dispose(True)
        ' TODO: uncomment the following line if Finalize() is overridden above.
        ' GC.SuppressFinalize(Me)
    End Sub
#End Region


End Class
