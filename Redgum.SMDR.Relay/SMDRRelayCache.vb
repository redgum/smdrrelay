﻿Imports Biggy.Core
Imports Biggy.Data.Json

Public Class SMDRRelayCache
    Private _Store As JsonStore(Of SMDRRelayCacheItem)
    Private _Cache As BiggyList(Of SMDRRelayCacheItem)
    Private _CacheLimit As Integer

    Public Sub New(appDataFolderName As String, cacheId As String, cacheLimit As Integer)
        MyBase.New()
        _IsInitialised = False
        _CacheLimit = cacheLimit

        Dim appData As String
        If IO.Path.IsPathRooted(appDataFolderName) Then
            appData = appDataFolderName
        Else
            appData = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), appDataFolderName)
        End If

        _Store = New JsonStore(Of SMDRRelayCacheItem)(appData, "cache", cacheId.Trim)
        _Cache = New BiggyList(Of SMDRRelayCacheItem)(_Store)
    End Sub
    
    Public Function AddRecord(smdrRecord As String) As SMDRRelayCacheItem
        If Cache.Count > _CacheLimit Then
            'Need to nuke the first records until we get to the limit
            Dim items = Cache.OrderBy(Function(x) x.ReceivedDateTime).Take(Cache.Count - _CacheLimit + 1)
            Cache.Remove(items)
        End If

        Dim item = New SMDRRelayCacheItem(smdrRecord)
        Cache.Add(item)
        Cache.Update(item)

        Return item
    End Function

    Public Sub RemoveRecord(record As SMDRRelayCacheItem)
        Cache.Remove(record)
    End Sub

    Public ReadOnly Property Cache As BiggyList(Of SMDRRelayCacheItem)
        Get
            Return _Cache
        End Get
    End Property

    Private _IsInitialised As Boolean
    Public ReadOnly Property IsInitialised As Boolean
        Get
            Return _IsInitialised
        End Get
    End Property
End Class

Public Class SMDRRelayCacheItem
    Public Sub New()
        MyBase.New()

        Me.ReceivedDateTime = DateTime.Now
    End Sub

    Public Sub New(data As String)
        Me.New()

        Me.Data = data
    End Sub

    Public Property Data As String
    Public Property ReceivedDateTime As DateTime
    Public Property SendAttempts As Integer
End Class