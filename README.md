# Redgum SMDR Relay#

### What is the SMDR Relay? ###

The SMDR relay does only one thing - acts as a distribution endpoint for an SMDR capable phone system, and rebroadcasts the SMDR message out to multiple endpoints.

### How do I get set up? ###

Grab the source, open in Visual Studio 2012 or 2013, and compile. Nuget will magic in the missing pieces, and you should end up with a small console application.

So far, it has only been tested with an Avaya IP Office 500 system. Given that SMDR formats can vary, your mileage may vary also.

### Configuring the endpoint ###

The relay requires an endpoint port that it will receive the incoming SMDR data on, and optionally an IP address for the machine (however leaving the IP address blank will allow the service to catch SMDR records on the specified port for all interfaces on the computer

For the outbound endpoints, these are configured in the app.config file as an IP:Port definition, eg. `192.168.26.1:3000

Add in as many of those endpoints as you need - each one will be called for every received SMDR message

### Contribution guidelines ###

This project is maintained by the developers at Redgum Technologies in Melbourne, Australia. It was a little app we needed one day while testing our phone and time management systems, and it seemed like it might be handy. Feel free to fork and submit pull requests

We are happy to accept pull requests for any changes or improvements.